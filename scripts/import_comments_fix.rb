require 'date'
require 'csv'
require 'rainbow'
floc = File.expand_path("~/Downloads/P11073-10201/P11073-10201_comments-f.csv")
csv = CSV.table(floc)
[:comment_id, :date, :number, :name, :email, :phone, :style, :index, :classification, :vote, :affiliation, :category, :page, :subclause, :line, :comment, :file, :must_be_satisfied, :proposed_change, :disposition_status, :disposition_detail, :other1, :other2, :other3]

orgs   = []
people = []
peorgs = {}

csv.each do |row|
  next if row[:number].to_s.strip.empty?
  ChangeTracker.start 
  cmt = IEEE::Comments::Comment.where(:number => row[:number].strip.chomp).first
  puts Rainbow("Can't find existing #{row[:number]}").magenta unless cmt
  comment = row[:comment]
  puts comment.inspect;puts
  cmt.comment = Gui_Builder_Profile::RichText.new(:content => comment)
  cmt.save
  ChangeTracker.commit
end
