require 'csv'
module IEEE
  module Comments
    class Ballot
      def to_csv
        csv = ["Comment ID", "Date", "Number", "Name", "Email", "Phone", "Style", "Index #", "Classification", "Vote", "Affiliation", "Category", "Page", "Subclause", "Line", "Comment", "File", "Must Be Satisfied", "Proposed Change", "Disposition Status", "Disposition Detail", "Other1", "Other2", "Other3"].to_csv
        comments.sort_by { |c| c.number.slice(/\d+/).to_i }.each { |c| csv << c.to_csv }
        csv
      end
      
      derived_attribute :download_csv, File
      def download_csv
        filename = "Ballot_#{standard.name}_version_#{revision}_#{Time.now.strftime('%d-%b-%Y_%l:%M:%S')}.csv"
        {:content => to_csv, :filename => filename, :mime_type => 'text/csv'}
      end
      
      derived_attribute(:summary_html, ::String)
      def summary_html
        html = []
        html << "<h6>#{standard.name}</h6><br>"
        html << '<p>'
        html << "Revision:&nbsp;         #{revision}<br>"
        html << "Opened:&nbsp;           #{opened.strftime("%B %-d, %Y %l:%M:%S%P").gsub('  ', ' ')}<br>"
        html << "Closed:&nbsp;           #{closed.strftime("%B %-d, %Y %l:%M:%S%P").gsub('  ', ' ')}<br>"
        html << '</p><br>'
        html.join
      end
      alias_association(:must_be_satisfied, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:must_be_satisfied => true})
      alias_association(:editorials, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:category_id => 1})
      alias_association(:technicals, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:category_id => 2})
      alias_association(:generals, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:category_id => 3})
      alias_association(:resolved, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:resolved => true})
      alias_association(:unresolved, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:resolved => false})
      alias_association(:unexamined, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:disposition_id => nil})
      alias_association(:accepted, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:disposition_id => 1})
      alias_association(:revised, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:disposition_id => 2})
      alias_association(:rejected, 'IEEE::Comments::Comment', :type => :one_to_many, :alias_of => :comments, :filter => {:disposition_id => 3})
      
    end
    class Comment
      def classification
        "FIXME"
      end
      def to_csv
        # csv = ["Comment ID", "Date", "Number", "Name", "Email", "Phone", "Style", "Index #", "Classification", "Vote", "Affiliation", "Category", "Page", "Subclause", "Line", "Comment", "File", "Must Be Satisfied", "Proposed Change", "Disposition Status", "Disposition Detail", "Other1", "Other2", "Other3"].to_csv
        per = person
        lfname = per.last_name + ', ' + per.first_name
        correct_ballot = person.ballots_associations.find { |b| b[:to] == ballot }
        # This bit may change if the model changes to accout for non-voter associations to a ballot
        vote  = correct_ballot ? correct_ballot[:through].vote&.value : 'Non-voter'
        dispo = disposition ? disposition.value : ''
        fname = file ? file_filename : ''
        discuss = (discussion_content && !discussion_content.strip.empty?) ? ("Comment Resolution Group Notes and Discussion:\n " + discussion_content.to_s) : ''
        csv = [comment_id.to_s, date.strftime('%d-%b-%Y %l:%M:%S ET'), number.to_s, lfname, per.email_address, per.phone, style.to_s, index.to_s, per.classification&.value, vote, per.affiliation&.name, category&.value, page.to_s, subclause, line.to_s, comment_content, fname, must_be_satisfied ? 'Yes' : 'No', proposed_change_content, dispo, disposition_detail_content, discuss, '', ''].to_csv
        csv
      end
      
      derived_attribute(:summary_html, ::String)
      def summary_html
        commenter = person
        made_by = person.first_name + ' ' + person.last_name
        html = []
        html << '<p>'
        html << "Subclause:&nbsp;         #{subclause}<br>"
        html << "Page:&nbsp;              #{page}<br>"
        html << "Line:&nbsp;              #{line}<br>"
        html << "Date:&nbsp;              #{date.strftime("%B %-d, %Y %l:%M:%S%P").gsub('  ', ' ')}<br>"
        html << "Number:&nbsp;            #{number}<br>"
        html << "Category:&nbsp;          #{category.value}<br>"
        html << "Must Be Satisfied:&nbsp; #{must_be_satisfied ? 'Yes' : 'No'}<br>"
        html << "Made By:&nbsp;           #{made_by}<br>"
        html << '</p><br>'
        # html.join("<br>")
        html.join
      end
    end
  end
end
