not_admin = proc { !Ramaze::Current.session[:user].site_admin? }
organizer(:Details, Home) {
  # html(:label => 'current', :html => proc{
  #   haml = <<-EOS
  #   :sass
  #     .div_style
  #       :color red
  #       :font-size 32px
  #       :text-align center
  #       :display block
  #
  #   %div.div_style
  #     %a.a_style{:href => '/IEEE/Comments/Ballot/1'} Go to Current Ballot
  #   EOS
  #   Haml::Engine.new(haml.reset_indentation).render(self)
  # })
  html(:label => 'current', :html => proc {
    haml = <<-EOS
    :sass
      $color:   #84f9eb
        
      .div_style
        :text-align center
        :display block
        :font-size 24px
        :background-color #053aaa
        :border-radius 5px
        a
          color: $color
          text-align: center
          text-decoration: none
          margin-left: 20px

          &:visited
            color: #9cd5f3
          &:hover
            color: $color

        
    %div.div_style
      %a.a_style{:href => '/IEEE/Comments/Ballot/1'} --> Go to Current Ballot <--

    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
  order('current','standards', 'comments', 'organizations', 'people')
}

organizer(:Details, IEEE::Comments::Ballot) {
  button('download_csv', :label => 'Current CSV', :button_text => 'Download CSV', :display_result => :file)
  relabel('comments', 'All Comments')
  relabel('people', 'Participants')
  message('summary_html')
  disable_if(not_admin, 'balloted_document', 'working_draft', 'people')
  hide_if(not_admin, 'opened', 'closed', 'revision', 'standard', 'comment_groups', 'people')
  view_ref(:Summary,'must_be_satisfied', :label => '"Must Be Satisfied" Comments')
  view_ref(:Summary,'editorials', :label => 'Editorial Comments')
  view_ref(:Summary,'technicals', :label => 'Technical Comments')
  view_ref(:Summary,'generals', :label => 'General Comments')
  view_ref(:Summary,'accepted', :label => 'Accepted Comments')
  view_ref(:Summary,'revised', :label => 'Revised Comments')
  view_ref(:Summary,'rejected', :label => 'Rejected Comments')
  view_ref(:Summary,'unexamined', :label => 'Unexamined Comments')
  view_ref(:Summary,'resolved', :label => 'Resloved Comments')
  view_ref(:Summary,'unresolved', :label => 'Unresloved Comments')
  reorder('summary_html', 'people', 'ballotted_document', 'working_draft', 'download_csv', 'comments', 'must_be_satisfied', 'editorials', 'technicals', 'generals', 'must_be_satisfied', 'accepted', 'revised', 'rejected', 'unexamined', 'resolved', 'unresolved', 'comment_groups', 'standard', :to_beginning => true)
}

collection(:Summary, IEEE::Comments::Comment) {
  hide('comment_id', 'style', 'index', 'discussion')
  order('must_be_satisfied', 'disposition', 'resolved', 'category', 'number', 'subclause', 'page', 'line', 'comment', 'proposed_change', 'disposition_detail', 'date')
}

organizer(:Details, IEEE::Comments::Comment) {
  message('summary_html')
  reorder('summary_html', 'disposition', 'resolved', 'comment', 'proposed_change', 'disposition_detail', 'discussion', 'file', 'person', 'ballot', 'duplicate_group', :to_beginning => true)
  hide('comment_id', 'style', 'index')
  hide('page', 'subclause', 'line', 'date', 'number', 'category',  'must_be_satisfied')
  # hide_if(not_admin, 'page', 'subclause', 'line', 'date', 'number', 'category',  'must_be_satisfied')
  disable_if(not_admin, 'page', 'subclause', 'line', 'date', 'number', 'category', 'comment', 'proposed_change', 'must_be_satisfied', 'ballot', 'person')
  expand('discussion', 'disposition_detail')
}

organizer(:Summary, IEEE::Stakeholder::Person) {
  disable_all
}

global_spec_modifier do |view|
  # disable all fields in organizer summary widgets
  if view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Summary)
    getters = view.content.collect{|w| w.getter.to_s}
    view.disable(*getters)
  end
end

