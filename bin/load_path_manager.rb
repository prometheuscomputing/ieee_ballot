# Foundation load_path_management can not be revectored to a development version, becasue it's code is used to do the redirection (to reduce redundancy)
require 'Foundation/load_path_management'
require 'rainbow'

# NOTE: In some versions of Rubygems there is a bug where the load path is ignored if there is only one version of a gem installed.
# A workaround for the problem where the local project is not used in favor of an installed gem is to make sure there are at least two versions of the gem installed.

module LoadpathManager
  def self.amend_load_path
    # # Put local car_example folder on load path
    $:.unshift relative('../../car_example/lib') # TODO setup_up should handle this if passed a grandchild file.
    # Put gui_site on load path
    $:.unshift relative('../lib')
    # Put local repos for gui_director and html_gui_builder on load path
    $:.unshift relative('../../gui_director/lib')
    $:.unshift relative('../../gui_widgets/lib')
    $:.unshift relative('../../html_gui_builder/lib')
    # $:.unshift relative('../../caracal/lib')
    # Put local repo for common on load path
    $:.unshift relative('../../common/lib')
    # Use local SSA/SCT versions
    $:.unshift relative('../../sequel_specific_associations/lib')
    require 'sequel_specific_associations'
    # $:.unshift relative('../../sequel_change_tracker/lib')
    # Use local SPM/Tyrant versions
    # $:.unshift relative('../../tyrant/lib')
    # $:.unshift relative('../../sequel_policy_machine/lib')
    # For testing new car_example_generated code
    car_example_generated_folder = File.expand_path("~/Prometheus/Generated_Code/car_example_generated/lib")
    # $:.unshift car_example_generated_folder
  end
  def self.display_paths
    puts Rainbow("\nProjects running from development directory:").magenta
    found = $:.select do |path|
      project = /(?<=projects\/).+/.match(path)
      puts Rainbow("    #{project.to_s.gsub(/\/lib.*/, '')}").yellow if project
      project
    end
    puts Rainbow("    none").green if found.empty?
  end
end
