#!/usr/bin/env ruby
require_relative 'load_path_manager'
if ARGV[0] =~ /test|dev|local/i
  ARGV.shift
  LoadpathManager.amend_load_path
end
LoadpathManager.amend_load_path
LoadpathManager.display_paths

application_module = Foundation.setup_app('ieee_ballot', __FILE__)
# require 'sequel_specific_associations'
# require 'sequel_change_tracker'
# require 'dim_generated'
# require 'dim_generated/model/driver'
# require 'dim/model_extensions'
#
require 'gui_site/launcher'
require 'gui_director'
require 'html_gui_builder'
Gui.run(application_module, :server => :setup_only)
require 'gui_site/server_config'

# load 'scripts/sync.rb' if MyDevice::PCDProfile.count == 0
ChangeTracker.delete_all_history
# migrate_all_attr_usage_data
# ChangeTracker.start


