require 'date'
require 'csv'
require 'rainbow'

def ct
  ChangeTracker.start
  yield if block_given?
  ChangeTracker.commit
end


floc = File.expand_path("~/Downloads/P11073-10201/P11073-10201_comments-f.csv")
csv = CSV.table(floc)
[:comment_id, :date, :number, :name, :email, :phone, :style, :index, :classification, :vote, :affiliation, :category, :page, :subclause, :line, :comment, :file, :must_be_satisfied, :proposed_change, :disposition_status, :disposition_detail, :other1, :other2, :other3]

orgs   = []
people = []
peorgs = {}

standard = IEEE::Comments::Standard.first
unless standard
  ChangeTracker.start
  standard = IEEE::Comments::Standard.create(:name => '11073-10201')
  ChangeTracker.commit
end

p "Revision of the ballot?: "
bname = gets.chomp
ChangeTracker.start
ballot = IEEE::Comments::Ballot.create(:revision => bname)
ChangeTracker.commit
ChangeTracker.start
standard.ballots_add(ballot)
standard.save
ChangeTracker.commit

csv.each do |row|
  next if row[:number].to_s.strip.empty?
  af = row[:affiliation].to_s.strip.chomp
  unless af.empty?
    if orgs.include?(af)
      org = IEEE::Stakeholder::Organization.where(:name => af).first
    else
      orgs << row[:affiliation].strip.chomp
      ct {
        org = IEEE::Stakeholder::Organization.new(:name => af)
        org.save      
      }
    end
  end
  lname, fname = row[:name].strip.chomp.split(/, /)
  if people.include?(row[:name].strip.chomp)
    person = IEEE::Stakeholder::Person.where(:last_name => lname).first
  else
    people << row[:name].strip.chomp
    ChangeTracker.start
    person = IEEE::Stakeholder::Person.create(:last_name => lname, :first_name => fname, :email_address => row[:email].strip.chomp,
      :phone => row[:phone])
    classification = IEEE::Comments::VoterClass.where(:value => row[:classification].to_s.strip.chomp).first
    puts Rainbow("WHOA, can't find classification #{row[:classification]} for comment #{row[:number].strip.chomp}").red unless classification
    person.classification = classification if classification
    person.affiliation = org
    person.add_ballot(ballot)
    person.save
    ChangeTracker.commit
  end

  ChangeTracker.start 
  cmt = IEEE::Comments::Comment.create(
    :comment_id    => row[:comment_id],
    :date          => DateTime.parse(row[:date].strip.chomp),
    :number        => row[:number].strip.chomp,
    :style         => row[:style].strip.chomp,
    :index         => row[:index],
    :page          => row[:page],
    :subclause     => row[:subclause],
    :line          => row[:line],
    :must_be_satisfied => row[:must_be_satisfied].strip.chomp == 'Yes'
  )
  cat = IEEE::Comments::CommentCategory.where(:value => row[:category].strip.chomp).first
  puts Rainbow("WHOA, can't find #{row[:category]} for comment #{row[:number]}").yellow unless cat
  cmt.category = cat if cat
  cmt.comment = Gui_Builder_Profile::RichText.new(:content => row[:content])
  cmt.proposed_change = Gui_Builder_Profile::RichText.new(:content => row[:proposed_change])
  puts Rainbow("Attach file manually for comment #{row[:number]}").magenta unless row[:file].to_s.strip.empty?
  cmt.person = person
  cmt.save
  ChangeTracker.commit
  ChangeTracker.start
  ballot.comments_add(cmt)
  ballot.save
  ChangeTracker.commit
end
